import { Router } from 'express'

const ObjectId = require('mongodb').ObjectID;


const asistentes = [
	{
		id	: 0,
		nombre	: 'Sebastian',
		apellido: 'Alvarez',
		email	: 'seba.mail@server.com'
	},
	{
		id	: 1,
		nombre	: 'Sebastian',
		apellido: 'Castrillo',
		email	: 'castrillo.mail@server.com'
	}

]

export default (db) => {
	const api = Router()

	api.get('/asistentes/', (req, res) => {

		const collection = db.collection('asistentes')
		collection.find().toArray( (err, doc) => {
			res.json(doc)
		})

	})

	api.get('/asistentes/:email', (req, res) => {
		const email = req.params.email

		const collection = db.collection('asistentes')
		collection.findOne({email: email}, (err, doc) => {
			res.json(doc)
		})
		
	})

	api.post('/asistentes/', (req, res) => {
		const asistente = req.body

		if(!asistente.email) {
			return res.json({ok: false, msg: 'Falta email'})
		}

		const collection = db.collection('asistentes')
		collection.insert(asistente)

		res.json({ok: true, msg: 'Asistente agregado correctamente'})
	})

	/*
	api.delete('/asistentes/:id', (req, res) => {
		const id = req.params.id

		const collection = db.collection('asistentes')
		collection.remove({_id: ObjectId(id)}, (err, result) => {
			return res.json(result)
		})
	})
	*/

	return api
}
