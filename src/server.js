import path from 'path'

import express from 'express'
import bodyParser from 'body-parser'
import mongosking from 'mongoskin'
import api from './api'


const port = 8080
const MongoClient = require('mongodb').MongoClient
const app = express()

const url = 'mongodb://localhost:27017/almibar';


const db = mongosking.db(url, {native_parser:true})


app.use(express.static('./public'))

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api', api(db))

app.get('/', (req, res) => {
	res.sendFile(path.join(__dirname + '/index.html'))

})
app.listen(port, () => {
	console.log(`almibar iniciado en ${port}`)
})


module.exports = app


