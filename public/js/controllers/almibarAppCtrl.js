angular.module('mainCtrl', [])
.controller('mainController', function($scope, $http, $document) {

	function _fetchData(url, cb) {
		$http.get(url).then(function(data) {
			return cb(data.data)
		});
	}

	function _sendData(url, data) {

		$http.post(url, data).then(function(res) {
			console.log(res)
		});

	}

	$scope.nuevoAsistente = function() {

		var asistente = {
			nombre: $scope.uiData.nombre,
			apellido: $scope.uiData.apellido,
			email: $scope.uiData.email
		};

		_sendData($scope.urls.asistentesTodos, asistente);
	}

	$scope.ui = {};
	$scope.urls = {};
	$scope.uiData = {};

	$scope.uiData.nombre;
	$scope.uiData.apellido;
	$scope.uiData.email;


	$scope.origData;
	$scope.filtered;
	


	$scope.ui.title = 'Almíbar';

	$scope.urls.asistentesTodos = '/api/asistentes/';

	$document.ready(function () {
		
		_fetchData($scope.urls.asistentesTodos, function(data) {
			$scope.origData = data;
			$scope.filtered = data;
		});
	})

})

