# 3b-almibar

## Introducción
3b-almibar es un proyecto destinado a enseñar las bases del stack MEAN.

## Tecnologías
 - MongoDB (para este proyecto es necesario tener localmente corriendo una instancia de mongo)
 - Express
 - AngularJS
 - NodeJS


## Branches
 - master
 - ruteo-cliente

**Es probable que tengan que instalar dependencias nuevas cuando cambian de branch, por ejemplo
el branch _ruteo-cliente_ necesita _angular-ui-router_, que el master branch no requiere**

### Cambio de branch
```sh
$ git checkout nombre-branch
```

### Push a branch específico
```sh
$ git push origin nombre-branch
```

## Instalación de dependencias

```sh
$ npm install
$ node_modules/.bin/bower install
```

## Entornos de ejecución
### Desarrollo
```sh
$ npm run dev
```

### Producción
```sh
$ npm run prod
```
