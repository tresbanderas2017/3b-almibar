const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../src/server');
const should = chai.should();

chai.use(chaiHttp);


describe('/GET asistentes', () => {

	it('Deberia devolver todos los asistentes', (done) => {
		chai	.request(server)
			.get('/api/asistentes')
			.end( (err, res) => {
				res.should.have.status(200)
				res.body.should.be.a('array');
				done()
			})
	})
})


describe('/POST asistentes', () => {
	it('Deberia devolver ok igual a true', (done) => {
		chai	.request(server)
			.post('/api/asistentes/')
			.send({nombre: 'Pablo', apellido: 'Carrai', email: 'pablo.mail@server.com'})
			.end( (err, res) => {
				res.body.should.be.a('object');
				res.should.have.status(200)
				res.body.should.have.property('ok').eql(true);
				done()
			})
	}),

	it('Deberia devolver ok igual a false', (done) => {
		chai	.request(server)
			.post('/api/asistentes/')
			.send({nombre: 'Sebastian', apellido: 'Alvarez'})
			.end( (err, res) => {
				res.body.should.be.a('object');
				res.should.have.status(200)
				res.body.should.have.property('ok').eql(false);
				done()
			})
	})
})
